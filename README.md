# README #

This project calculates timing of catching exceptions locally within a function and globally in the calling function.
The results are varied depending on how the calling function is arranged. The code is written in C++.
This code can be reused and redistributed as freeware.