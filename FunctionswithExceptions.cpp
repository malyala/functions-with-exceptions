/***********************************************************
  Name: FunctionswithExceptions.cpp
  Author: Amit Malyala                                     
  Copyright:  Freeware
   Description:
   Write a program consisting of functions calling each other. Give each function an argument that determines at 
   which level an exception is thrown. Have main() catch these  exceptions and print out which exception is caught. Don�t forget the case 
   in which an exception is caught in the function that throws it. 
   This can mean that a exception is thrown at any level and it is caught in main() or in the function itself
   Notes:
   These functions can be made reentrant.
   In one case, the exceptions are caught in calling function and in the second case, the exceptions are caught in the function too.
   Exception catch timings cant be calculated in single runs. Run atleast 1000 times and print average. Single run is using cold cache, all other runs
   are using hot cache giving nearly 0 nano seconds execution times with GCC 9.2. With gcc 4.9.0 the results are varied.
   Adding a std::string member seems to increase the timing for catching a exception slightly 
   
   Version and bug history:
   0.1 Initial version.  
   0.2 Completed definitions of funtions, added struct for func parameters, created nested catch blocks in main()
   0.3 Changed funtion 1 and 2 definitions, created CalcGlobalExceptionTime() functions.
   
   Coding log:
   13-11-20 Created initial version.
   17-11-20 Changed functions and added new function void CalcGlobalExceptionTime()
***********************************************************/
#include <iostream>
#include <string>
#include <chrono>

/* Some data about functions*/
struct FunctionParam {
    int level;
    int FuncNumber;
    std::string name {"FunctionData"};
};

// Function prototypes of function one and two.
// Take number of function call as parameter
void Functionone(int level);
// Take number of function call as parameter
void Functiontwo(int level);
/* Function 1 */
void Functionone(int level) {
    int l = 0;
    static long int sum = 0;
    static int count = 1;
    l = level;
    l++;
    FunctionParam Fparam;
    static bool funcSwitch =false;
    Fparam.level = l;
    Fparam.FuncNumber = 1;
    //std::cout << "Function one level " << l << '\n';
    if (funcSwitch)
    {
        funcSwitch=false;
		// Measure function execution time.
        auto start = std::chrono::steady_clock::now();
        try {

            throw Fparam;
        } catch (FunctionParam & Fparm) {
            //std::cout << "Exception at level " << Fparam.level << " in function call in Function " << Fparam.FuncNumber << '\n';
        }
        // Measure execution time.
        auto end = std::chrono::steady_clock::now();
        auto diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();
        //std::cout << "Execution time " << diff << " nanoseconds";
        sum += diff;
        count++;
        if (count == 1000) {
            std::cout << "Execution time of exception in Function 1 " << sum / 1000 << " nanoseconds" << '\n';
            count = 0;
            sum = 0;
        }
    }
	  else if (!funcSwitch) {
	  funcSwitch=true;
      throw Fparam;    
    }
    Functiontwo(l);    
}

/* Function 2 */
void Functiontwo(int level) {
    int l = 0;
    static int count = 1;
    l = level;
    static long int sum = 0;

    FunctionParam Fparam;
    Fparam.level = level;
    Fparam.FuncNumber = 2;
    static bool funcSwitch =false;
    //std::cout << "Function two level " << l << '\n';
    if (funcSwitch)
    {
        funcSwitch=false;
        // Measure function execution time.
        auto start = std::chrono::steady_clock::now();
        try {

            throw Fparam;
        } catch (FunctionParam & Fparm) {
            //std::cout << "Exception at level " << Fparam.level << " in function call in Function " << Fparam.FuncNumber << '\n';
        }
        // Measure execution time.
        auto end = std::chrono::steady_clock::now();
        auto diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();
        sum += diff;
        count++;
        if (count == 1000) {
            std::cout << "Execution time of exception in Function 2 " << sum / 1000 << " nanoseconds" << '\n';
            count = 0;
            sum = 0;
        }

    } 
    else if (!funcSwitch) {
	  funcSwitch=true;
	  throw Fparam;     		
    }
    Functionone(l); 
}

// Application program 
void ExecuteAPP(void);

/* Functin calculate global exeption timing */
void CalcGlobalExceptionTime(bool & flagf1, bool& flagf2,int& F1exceptioncount,int& F2exceptioncount, FunctionParam & Fparam, long int& f1sum,long int& f2sum, long int& diff);
// Main function 
int main(void) {

    ExecuteAPP();
    return 0;
}


/*
 Application program 
 to catch exceptions from funtion 1 and 2
 */
void ExecuteAPP(void)
{
	static long int f1sum=0;
    static long int f2sum=0;
    static long int diff=0;
    int F1exceptioncount = 0;
    int F2exceptioncount = 0;
    int Exceptionlevel = 0;
    bool flagf1=false;
    bool flagf2=false;
    for (int index = 0; index < 3000; index++) 
	{
        // Measure code block execution time.
        auto start = std::chrono::steady_clock::now();
        try {
            Functionone(Exceptionlevel);
        } catch ( FunctionParam &Fparam) {
            
            auto end = std::chrono::steady_clock::now();
            diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
            CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
            try {
                Exceptionlevel = Fparam.level;
                Functionone(Fparam.level);
            } catch (FunctionParam & Fparam) {
                auto end = std::chrono::steady_clock::now();
                diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
                // Add nested try catch blocks
                try {
                    Exceptionlevel = Fparam.level;
                    Functionone(Fparam.level);
                } catch (FunctionParam & Fparam) {
            
                    auto end = std::chrono::steady_clock::now();
                    diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                    CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);

                    // Add nested try catch blocks
                    try {
                        Exceptionlevel = Fparam.level;
                        Functionone(Fparam.level);
                    } catch (FunctionParam & Fparam) {
            
                        auto end = std::chrono::steady_clock::now();
                        diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                        CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
                        // Add nested try catch blocks
                        try {
                            Exceptionlevel = Fparam.level;
                            Functionone(Fparam.level);
                        } catch (FunctionParam & Fparam) {
            
                            auto end = std::chrono::steady_clock::now();
                            diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                            CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
                            // Add nested try catch blocks
                            try {
                                Exceptionlevel = Fparam.level;
                                Functionone(Fparam.level);
                            } catch (FunctionParam & Fparam) {
            
                                auto end = std::chrono::steady_clock::now();
                                diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);

                                // Add nested try catch blocks
                                try {
                                    Exceptionlevel = Fparam.level;
                                    Functionone(Fparam.level);
                                } catch (FunctionParam & Fparam) {
            
                                    auto end = std::chrono::steady_clock::now();
                                    diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                    CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
                                    // Add nested try catch blocks
                                    try {
                                        Exceptionlevel = Fparam.level;
                                        Functionone(Fparam.level);
                                    } catch (FunctionParam & Fparam) {
            
                                        auto end = std::chrono::steady_clock::now();
                                        diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                        CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);

                                        // Add nested try catch blocks
                                        try {
                                            Exceptionlevel = Fparam.level;
                                            Functionone(Fparam.level);
                                        } catch (FunctionParam & Fparam) {
            
                                            // Measure execution time.
                                            auto end = std::chrono::steady_clock::now();
                                            diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                            CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);

                                            // Add nested try catch blocks
                                            try {
                                                Exceptionlevel = Fparam.level;
                                                Functionone(Fparam.level);
                                            } catch (FunctionParam & Fparam) {
            
                                                // Measure execution time.
                                                auto end = std::chrono::steady_clock::now();
                                                diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                                CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);

                                                // Add nested try catch blocks
                                                try {
                                                    Exceptionlevel = Fparam.level;
                                                    Functionone(Fparam.level);
                                                } catch (FunctionParam & Fparam) {
                                                    // Measure execution time.
                                                    auto end = std::chrono::steady_clock::now();
                                                    diff = std::chrono::duration_cast < std::chrono::nanoseconds > (end - start).count();   
                                                    CalcGlobalExceptionTime(flagf1,flagf2,F1exceptioncount,F2exceptioncount,Fparam,f1sum,f2sum,diff);
                                                    // Add nested try catch blocks
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
}
}


/* Function calculate global exeption timing */
void CalcGlobalExceptionTime(bool & flagf1, bool& flagf2,int& F1exceptioncount,int& F2exceptioncount, FunctionParam & Fparam,long int& f1sum,long int& f2sum, long int& diff)
{
    if (Fparam.FuncNumber == 1) {
        F1exceptioncount++;
        flagf1=true;
        flagf2=false;
    } else {
        F2exceptioncount++;
        flagf2=true;
        flagf1=false;
    }
		
	if (flagf1)
	{
		f1sum+=diff;
	}
	if (flagf2)
	{
		f2sum+=diff; 
	}
	if (F1exceptioncount ==1000)
	{
		std::cout << "Execution time of catching Function 1 exception in another function " << f1sum / 1000 << " nanoseconds\n";
		F1exceptioncount=0;
		f1sum=0;
	}
	if (F2exceptioncount ==1000)
	{
		std::cout << "Execution time of catching Function 2 exception in another function " << f2sum / 1000 << " nanoseconds\n";
		F2exceptioncount=0;
		f2sum=0;
	}
	
    flagf1=false, flagf2=false;
}
#if(0)
// Execution time calculation
//auto start = std::chrono::steady_clock::now();

// Measure execution time.
//auto end = std::chrono::steady_clock::now();
//auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
//std::cout << "Execution time " << diff << " nanoseconds";
#endif
